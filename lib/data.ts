const data = {
  products: [
    {
      nama: 'Ayam Goreng',
      slug: 'ayam-goreng',
      kategori: 'Makanan',
      image: '/images/ayamgoreng.jpg',
      harga: 20000,
      rating: 4.5,
      numReviews: 8,
      countInStock: 20,
      deskripsi: 'Ayam goreng mantap',
      isFeatured: true,
      banner: '/images/ayamgoreng.jpg',
    },
    {
      nama: 'Lele Goreng',
      slug: 'lele-goreng',
      kategori: 'Makanan',
      image: '/images/lelegoreng.jpg',
      harga: 15000,
      rating: 4.7,
      numReviews: 10,
      countInStock: 25,
      deskripsi: 'Lele goreng mantap',
      isFeatured: true,
      banner: '/images/lelegoreng.jpg',
    },
    {
      nama: 'Ayam Bakar',
      slug: 'ayam-bakar',
      kategori: 'Makanan',
      image: '/images/ayambakar.jpg',
      harga: 21000,
      rating: 4.8,
      numReviews: 19,
      countInStock: 10,
      deskripsi: 'Bebek goreng mantap',
      isFeatured: true,
      banner: '/images/ayambakar.jpg',
    },
  ],
}

export default data

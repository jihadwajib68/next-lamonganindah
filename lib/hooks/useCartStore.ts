import { create } from 'zustand'
import { round2 } from '../utils'
import { OrderItem } from '../models/OrderModel'

type Cart = {
  items: OrderItem[]
  itemsHarga: number
  taxHarga: number
  shippingHarga: number
  totalHarga: number
}

const initialState: Cart = {
  items: [],
  itemsHarga: 0,
  taxHarga: 0,
  shippingHarga: 0,
  totalHarga: 0,
}

export const cartStore = create<Cart>(() => initialState)

export default function useCartService() {
  const { items, itemsHarga, taxHarga, shippingHarga, totalHarga } = cartStore()
  return {
    items,
    itemsHarga,
    taxHarga,
    shippingHarga,
    totalHarga,
    increase: (item: OrderItem) => {
      const exist = items.find((x) => x.slug === item.slug)
      const updateCartItems = exist
        ? items.map((x) =>
            x.slug === item.slug ? { ...exist, qty: exist.qty + 1 } : x
          )
        : [...items, { ...item, qty: 1 }]
      const { itemsHarga, shippingHarga, taxHarga, totalHarga } =
        calcHarga(updateCartItems)
      cartStore.setState({
        items: updateCartItems,
        itemsHarga,
        shippingHarga,
        taxHarga,
        totalHarga,
      })
    },
    decrease: (item: OrderItem) => {
      const exist = items.find((x) => x.slug === item.slug)
      if (!exist) return
      const updateCartItems =
        exist.qty === 1
          ? items.filter((x: OrderItem) => x.slug !== item.slug)
          : items.map((x) => (item.slug ? { ...exist, qty: exist.qty - 1 } : x))
      const { itemsHarga, shippingHarga, taxHarga, totalHarga } =
        calcHarga(updateCartItems)
      cartStore.setState({
        items: updateCartItems,
        itemsHarga,
        shippingHarga,
        taxHarga,
        totalHarga,
      })
    },
  }
}

const calcHarga = (items: OrderItem[]) => {
  const itemsHarga = round2(
      items.reduce((acc, item) => acc + item.harga * item.qty, 0)
    ),
    shippingHarga = round2(itemsHarga > 100 ? 0 : 100),
    taxHarga = round2(Number(0.15 * itemsHarga)),
    totalHarga = round2(itemsHarga + shippingHarga + taxHarga)
  return { itemsHarga, shippingHarga, taxHarga, totalHarga }
}

export type Product = {
  _id?: string
  nama: string
  slug: string
  image: string
  banner?: string
  harga: number
  deskripsi: string
  kategori: string
  rating: number
  numReviews: number
  countInStock: number
}

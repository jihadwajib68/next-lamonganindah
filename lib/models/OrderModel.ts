export type OrderItem = {
  nama: string
  slug: string
  qty: number
  image: string
  harga: number
}
